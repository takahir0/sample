package sampleApp.com.form;

import lombok.Data;

@Data
public class Submit2Form {
	
	private String message;
	
	Submit2Form(){
		this.setMessage("message from submitForm2");
	}
	
}
