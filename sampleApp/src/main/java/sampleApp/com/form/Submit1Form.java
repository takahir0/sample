package sampleApp.com.form;

import lombok.Data;

@Data
public class Submit1Form {
	
	private String message;
	
	Submit1Form(){
		this.message = "message from submitForm1";
	}
	
}
