package sampleApp.com.form;

import org.springframework.format.annotation.NumberFormat;

import lombok.Data;

@Data
public class Submit3Form {

	@NumberFormat
	private int input1;
	
	@NumberFormat
	private int input2;
	
}
