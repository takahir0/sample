package sampleApp.com.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import sampleApp.com.form.Submit1Form;
import sampleApp.com.form.Submit2Form;
import sampleApp.com.form.Submit3Form;
import sampleApp.com.service.common.AbstractBaseService;

@Controller
public class SampleController {
	
	@Autowired
    MessageSource messageSource;
	
	@Autowired
	AbstractBaseService<Submit1Form> submit1Service;
	
	@Autowired
	AbstractBaseService<Submit2Form> submit2Service;
	
	@RequestMapping("/")
	public ModelAndView init(ModelAndView mav) {
		mav.addObject("msg","これはThymeleafを使ったサンプルです。");
		mav.setViewName("view/index");
		return mav;
	}
	
	@RequestMapping("/action1")
	public ModelAndView action1(ModelAndView mav, Submit1Form form) {
		mav.addObject("msg", submit1Service.createMesasge(form));
		mav.setViewName("view/index");
		return mav;
	}
	
	@RequestMapping("/action2")
	public ModelAndView action2(ModelAndView mav, Submit2Form form) {
		mav.addObject("msg", submit2Service.createMesasge(form));
		mav.setViewName("view/index");
		return mav;
	}
	
	@RequestMapping("/action3")
	public ModelAndView action3(@ModelAttribute @Valid Submit3Form form, BindingResult result, ModelAndView mav, Errors errors) {
		
		if(errors.hasErrors()) {
			mav.addObject("msg", errors.getFieldError());
			mav.setViewName("view/index");
			return mav;
		}
				
		mav.addObject("result", form.getInput1() + form.getInput2());
		mav.setViewName("view/index");
		return mav;
	}
	
	@RequestMapping("/action4")
	public Object action4(HttpServletResponse res) throws IOException {
		
		String msg = messageSource.getMessage("test", new String[] {}, Locale.JAPAN);
		String msg2 = messageSource.getMessage("test", new String[] {}, Locale.ENGLISH);		
		res.setHeader("msg", URLEncoder.encode(msg, "UTF-8"));
		res.setHeader("msg2", URLEncoder.encode(msg2, "UTF-8"));
		String msg3 = "直接文字列";
		res.setHeader("msg3", URLEncoder.encode(msg3, "UTF-8"));
		res.setCharacterEncoding("Unicode");
		res.flushBuffer();
		
		return null;
	}

}