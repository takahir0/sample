package sampleApp.com.service.functions;

import org.springframework.stereotype.Service;

import sampleApp.com.form.Submit2Form;
import sampleApp.com.service.common.AbstractBaseService;

@Service
public class Submit2Service extends AbstractBaseService<Submit2Form> {

	@Override
	public String getMsg(Submit2Form form) {
		return form.getMessage();
	}

}
