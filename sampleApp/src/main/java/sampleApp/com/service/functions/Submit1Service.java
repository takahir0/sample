package sampleApp.com.service.functions;

import org.springframework.stereotype.Service;

import sampleApp.com.form.Submit1Form;
import sampleApp.com.service.common.AbstractBaseService;

@Service
public class Submit1Service extends AbstractBaseService<Submit1Form> {

	@Override
	public String getMsg(Submit1Form form) {
		return form.getMessage();
	}

}
